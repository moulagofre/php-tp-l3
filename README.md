# php-tp1

## TP2

ex2 : Statistiques de la page https://en.wikipedia.org/wiki/PHP.
- Compter le nombre de lignes de cette page.
- Compter le nombre total des lettres [A-Za-z].
- Compter le nombre total de chiffres.
- Compter le nombre d’occurrences du mot PHP.

ex3 : Traitement d’un fichier structuré de m colonnes par n lignes en suivant les
étapes ci-dessous.
1. Charger les n lignes dans un tableau.
2. Créer un tableau principal.
3. Pour chaque ligne, stocker les m colonnes dans un sous-tableau (n sous-tableaux
au total)
4. Stocker tous les n sous-tableaux dans le tableau principal en les triant par la
première colonne.
5. Afficher les données dans une table HTML.
