<?php
$content = file_get_contents('./all.txt');
$result = array();

foreach(preg_split('/(\n)/',$content) as $line){
  $subArray = array();
  foreach(preg_split('/(\s)/',$line) as $column){
    $subArray[] = $column;
  }
  array_pop($subArray);
  $result[$subArray[0]] = $subArray;
}
array_pop($result);
ksort($result);

$final = '<html><body><table>'.PHP_EOL;
foreach($result as $line){
  $final .= "<tr>".PHP_EOL;
  foreach($line as $item){
    $final .= "<td>${item}</td>".PHP_EOL;
  }
  $final .= "</tr>".PHP_EOL;
}
$final .= "</table></body></html>";

file_put_contents('./all.html',$final);
