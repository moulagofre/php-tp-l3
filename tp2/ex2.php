<?php
$curlSession = curl_init();
curl_setopt($curlSession, CURLOPT_URL, "https://en.wikipedia.org/wiki/PHP");
curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($curlSession);
$content = strip_tags($output);
echo $content;
$result = ['lines'=>0,'[A-Za-z]'=>0,'[0-9]'=>0,'PHP'=>0];
foreach(preg_split('/(\n)/',$content) as $line){
  $result['lines'] += 1;
}
$occurences = array();
preg_match_all('/([A-Za-z])/',$content,$occurences);
foreach($occurences as $item){
  foreach($item as $sub){
    $result['[A-Za-z]'] += 1;
  }
}
$occurences = array();
preg_match_all('/([0-9])/',$content,$occurences);
foreach($occurences as $item){
  foreach($item as $sub){
    $result['[0-9]'] += 1;
  }
}
$occurences = array();
preg_match_all('/(PHP)/',$content,$occurences);
foreach($occurences as $item){
  foreach($item as $sub){
    $result['PHP'] += 1;
  }
}
foreach($result as $key => $value){
  echo "${key} : ${value}".PHP_EOL;
}
curl_close($curlSession);
