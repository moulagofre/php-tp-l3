<?php

require_once "includes.php";

$code = $_SERVER["QUERY_STRING"];

if (!$code) {
    header("Location: index.php");
    exit();
}

try {
  $comment = $em->Indexes()->getByCode($code);
  $text = $comment->getComment()->getContent();
  $name = $comment->getName();
  $note = $comment->getNote();
  $date = $comment->getTime();
} catch (PDOException $error) {
  header("Location: index.php");
  exit();
}

require_once "content.phtml";
