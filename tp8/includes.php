<?php

require_once "Commons/EntitiesManager.class.php";
require_once "Commons/Repository/EntitiesRepository.class.php";
require_once "Commons/Repository/CommentsRepository.class.php";
require_once "Commons/Repository/IndexesRepository.class.php";
require_once "Commons/Entities/Entity.class.php";
require_once "Commons/Entities/Index.class.php";
require_once "Commons/Entities/Comment.class.php";

use Commons\EntitiesManager as EntitiesManager;
use Commons\Entities\Index as Index;
use Commons\Entities\Comment as Comment;

$em = new EntitiesManager();
