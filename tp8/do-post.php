<?php

require_once "includes.php";

$name = urlencode($_POST["name"]);
$note = $_POST["note"];
$text = $_POST["text"];
$time = time();

if (!isset($name) || !isset($note) || !isset($text)) {
    header("Location: index.php");
    exit();
}

if($em->isInError()){
  ?><p style="color: red;"><?=$em->getMessage()?><p><?php
  header("Location: index.php");
  exit();
}
try {
  $newComment = $em->createComment();
  $newComment->setName($name)
             ->setNote($note)
             ->setTime($time)
             ->getComment()->setContent($text);
  $newComment->save();
  $em->flush();
} catch (Exception $error) {
  header("Location: index.php");
  echo $error->getMessage();
  exit();
}

header("Location: index.php");
exit();
