<?php

namespace Commons\Repository;

use \Commons\Entities\Index as Index;
use \PDO as PDO;

class IndexesRepository extends EntitiesRepository {

  /* Constructeur
    @return IndexesRepository
  */
  function __construct(&$db, &$em){
    parent::__construct($db, $em);
  }

  /* Methode qui sauvegarde un index
    @parameter Index $index
    @return string
  */
  function persist(Index $index) {
    try {
      $req = $this->db->prepare("INSERT INTO comment_index(code,name,note,time) VALUES(:code,:name,:note,:time)");
      $req->execute(array(
        'code' => $index->getComment()->getCode(),
        'name' => $index->getName(),
        'note' => $index->getNote(),
        'time' => $index->getRawTime(),
      ));
      return $this->db->lastInsertId();
    } catch (PDOException $e) {
      throw new Exception($e->getMessage());
    }
  }

  /* Methode qui renvoie tous les commentaires
    @return Index[]
  */
  function getAll() {
    try {
      $req = $this->db->query(
        "SELECT I.code as code, I.note as note , I.name as name, I.time as date
        FROM comment_index I
        ORDER BY I.time"
      );
      $allComments = $req->fetchAll();
      $comments = array();
      foreach($allComments as $comment) {
        $fetchedComment = new Index($this->em->Indexes(),$this->em->Comments());
        $fetchedComment->setName($comment['name'])
                       ->setNote($comment['note'])
                       ->setTime($comment['date'])
                       ->getComment()->setCode($comment['code']);
        $comments[] = $fetchedComment;
      }
      return $comments;
    } catch (PDOException $e) {
      throw new Exception($e->getMessage());
    }
  }

  function getByCode(int $code) {
    try {
      $req = $this->db->query(
        "SELECT C.content as content, I.name as name, I.note as note, I.time as time
        FROM comment_content C
        INNER JOIN comment_index I ON I.code=C.code
        WHERE I.code=$code"
      );
      $comment = $req->fetch();
      $fetchedComment = new Index($this->em->Indexes(),$this->em->Comments());
      $fetchedComment->setName($comment['name'])
                     ->setNote($comment['note'])
                     ->setTime($comment['time'])
                     ->getComment()->setContent($comment['content']);
      return $fetchedComment;
    } catch (PDOException $e) {
      throw new Exception($e->getMessage());
    }
  }
}
