<?php

namespace Commons\Repository;

abstract class EntitiesRepository {

  /* DB ref
    @var PDO $db
  */
  protected $db;

  /* EntityManager ref
    @var EntityManager $em
  */
  protected $em;

  /* Constructeur
    @return EntitiesRepository
  */
  function __construct(&$db, &$em){
    $this->db = $db;
    $this->em = $em;
  }

}
