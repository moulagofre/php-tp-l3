<?php

namespace Commons\Repository;

use Commons\Entities\Comment as Comment;

class CommentsRepository extends EntitiesRepository {

  /* Constructeur
    @return CommentsRepository
  */
  function __construct(&$db, &$em){
    parent::__construct($db, $em);
  }

  /* Methode qui sauvegarde un comment content
    @parameter Comment $comment
    @return string
  */
  function persist(Comment &$comment) {
    $req = $this->db->prepare("INSERT INTO comment_content(content) VALUE(:content)");
    $req->execute(array(
      'content' => $comment->getContent(),
    ));
    return $this->db->lastInsertId();
  }
}
