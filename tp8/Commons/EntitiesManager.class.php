<?php

namespace Commons;

use Commons\Repository\IndexesRepository as IndexesRepository;
use Commons\Repository\CommentsRepository as CommentsRepository;
use Commons\Entities\Index as Index;
use \PDO as PDO;

class EntitiesManager {

  private $dbAdress = 'mysql';
  private $dbName = 'pws';
  private $dbUser = 'pws';
  private $dbPassword = 'pws';

  /* Error indicator
    @var Error $error
  */
  private $error = null;

  /* Database PDO object
    @var PDO
  */
  private $db;

  /* IndexesRepository
    @var IndexesRepository $indexes
  */
  public $indexes;

  /* CommentsRepository
    @var CommentsRepository $indexes
  */
  public $comments;

  /* Constructeur
  */
  function __construct() {
    $this->dbInit();
    $this->indexes = new IndexesRepository($this->db,$this);
    $this->comments = new CommentsRepository($this->db,$this);
    $this->db->beginTransaction();
  }

  function Indexes() {
    return $this->indexes;
  }

  function Comments() {
    return $this->comments;
  }

  function createComment() {
    if(isset($error)) {
      throw $error;
    }
    return new Index($this->indexes,$this->comments);
  }

  function flush() {
    if(isset($error)) {
      throw $error;
    }
    try {
      $this->db->commit();
      $this->db->beginTransaction();
    } catch (PDOException $e) {
      $this->error = $e;
    }
  }

  function rollback() {
    if(isset($error)) {
      throw $error;
    }
    try {
      $this->db->rollback();
    } catch (PDOException $e) {
      $this->error = $e;
    }
  }

  function isInError() {
    return $error == null ? false : true;
  }

  private function dbInit() {
    try {
      $this->db = new PDO('mysql:host='.$this->dbAdress.';dbname='.$this->dbName, $this->dbUser, $this->dbPassword);
      $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
      throw new Exception("Erreur PDO!: ". $e->getMessage() ."<br/>");
    }
  }
}
