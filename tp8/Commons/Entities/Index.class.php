<?php

namespace Commons\Entities;

use Commons\Repository\IndexesRepository as IndexesRepository;
use Commons\Repository\CommentsRepository as CommentsRepository;

class Index extends Entity{

  /* RepositoryRef
    @var IndexesRepository $repo
  */
  private $repo;

  /* @var int $id */
  private $id;
  /* @var string $name */
  private $name;
  /* @var int $note */
  private $note;
  /* @var string $time */
  private $time;
  /* @var Index $comment
    Comment ForeignKey
  */
  private $comment;

  /* Constructeur
    @return Index
  */
  function __construct(IndexesRepository &$indexesRepo, CommentsRepository &$commentsRepo) {
    $this->repo = $indexesRepo;
    $this->initContent($commentsRepo);
  }

  /* Methode qui retourne le nom de l'auteur du commentaire
    @return string
   */
  function getId() {
    return $this->id;
  }

  /* Methode qui retourne le nom de l'auteur du commentaire
    @return string
   */
  function getName() {
    return $this->name;
  }

  /* Methode qui set le nom de l'auteur du commentaire
    @parameter string $value
    @return Index
  */
  function setName(string $value) {
    $this->name = $value;
    return $this;
  }

  /* Methode qui retourne la note associee au commentaire
    @return int
  */
  function getNote() {
    return $this->note;
  }

  /* Methode qui set la note associee au commentaire
    @parameter int $value
    @return Index
  */
  function setNote(int $value) {
    $this->note = $value;
    return $this;
  }

  /* Methode qui retourne la date formatee associee au commentaire
    @return string
  */
  function getTime() {
    return date("h:m D M Y",$this->time);
  }

  /* Methode qui retourne la date associee au commentaire
    @return string
  */
  function getRawTime() {
    return $this->time;
  }

  /* Methode qui set la date associee au commentaire
    @parameter string $value
    @return Index
  */
  function setTime(string $value) {
    $this->time = $value;
    return $this;
  }

  /* Methode qui retourne le commentaire
    @return Comment
  */
  function getComment() {
    return $this->comment;
  }

  /* Methode qui sauvegarde l'index en bdd
    @return void
  */
  function save() {
    $this->comment->save();
    try {
      $this->repo->persist($this);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }

  }

  private function initContent(CommentsRepository &$commentsRepo) {
    $this->comment = new Comment($commentsRepo);
  }
}
