<?php

namespace Commons\Entities;

use Commons\Repository\CommentsRepository as CommentsRepository;

class Comment extends Entity {

  /* RepositoryRef
    @var CommentsRepository $repo
  */
  private $repo;

  /* @var int $code */
  private $code = null;
  /* @var string $name */
  private $content = "";

  /* Constructeur
    @return Comment
  */
  function __construct(CommentsRepository &$commentsRepo) {
    $this->repo = $commentsRepo;
  }

  /* Methode qui retourne le code du commentaire
    @return string
   */
  function getCode() {
    return $this->code;
  }

  /* Methode qui set le code du commentaire
    @parameter string $value
    @return Index
  */
  function setCode(string $value) {
    $this->code = $value;
    return $this;
  }

  /* Methode qui retourne la contenu du commentaire
    @return int
  */
  function getContent() {
    return $this->content;
  }

  /* Methode qui set le contenu du commentaire
    @parameter string $value
    @return Index
  */
  function setContent(string $value) {
    $this->content = $value;
    return $this;
  }

  /* Methode qui sauvegarde le commentaire en bdd
    @return void
  */
  function save() {
    try {
      $this->code = $this->repo->persist($this);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }
}
